﻿using BooksApiRest.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BooksApiRest.Services
{
    public class AministrationService
    {

        private readonly IMongoCollection<Aministration> _adminstration;

        public AministrationService(IBookstoreDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);
            _adminstration = database.GetCollection<Aministration>(settings.AministrationCollectionName);
        }

        public List<Aministration> Get() =>
         _adminstration.Find(admin => true).ToList();

        public Aministration Get(string id) =>

        _adminstration.Find<Aministration>(admin=> admin.Id == id).FirstOrDefault();



        public void Update(string id, Aministration adminIn) =>
            _adminstration.ReplaceOne(admin => admin.Id == id, adminIn);
        public Aministration Auth(string email)
        {
            List<Aministration> lista = this.Get();
          //  Aministration auth = _adminstration.Find<Aministration>(Thisadmin => Thisadmin.email == email).FirstOrDefault();
            foreach (Aministration item in lista)
            {
                if(item.email==email)
                {
                    return item;
                }
            }
                return null;
        }
    }
}
