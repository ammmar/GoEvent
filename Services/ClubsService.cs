﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using BooksApiRest.Models;
using MongoDB.Bson;

namespace BooksApiRest.Services
{
    public class ClubsService
    {

        private readonly IMongoCollection<Club> _clubs;


        public ClubsService(IBookstoreDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);
            _clubs = database.GetCollection<Club>(settings.ClubsCollectionName);


        }



        public List<Club> Get() =>
         _clubs.Find(club => true).ToList();
        // _clubs.Find<Club>(ThisClub => ThisClub.Email == club.Email);




        public Club Get(string id) =>

                _clubs.Find<Club>(club => club.Id == id).FirstOrDefault();


        public Club Create(Club club)
        {
            if (_clubs.Find<Club>(ThisClub => ThisClub.Email == club.Email).FirstOrDefault() == null)
            {
                _clubs.InsertOne(club);

                return club;
            }
            else
            {
                return null;
            }

        }




        public void Update(string id, Club clubIn) =>
            _clubs.ReplaceOne(club => club.Id == id, clubIn);
        /*

        public bool updatePhoto(string id, string url)
        {
            Club myClub = _clubs.Find<Club>(club => club.Id == id).FirstOrDefault();
            if (myClub != null)
            {
                myClub.Logo = url;
                _clubs.ReplaceOne(club => club.Id == id, myClub);
                return true;

            }
            return false;
        }

    */
    /*
        public void Remove(Club clubIn) =>
            _clubs.DeleteOne(club => club._id == clubIn._id);*/

        public void Remove(string id) =>
            _clubs.DeleteOne(club => club.Id == id);



        public Club Auth(string email)
        {
            Club auth = _clubs.Find<Club>(ThisClub => ThisClub.Email == email).FirstOrDefault();
            return auth;
        }
    }
}
