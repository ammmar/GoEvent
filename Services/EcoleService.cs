﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using BooksApiRest.Models;
namespace BooksApiRest.Services
{
    public class EcoleService
    {
        private readonly IMongoCollection<ecole> _ecole;
        public EcoleService(IBookstoreDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);
            _ecole = database.GetCollection<ecole>(settings.EcoleCollectionName);

        }


        public List<ecole> Get() =>
         _ecole.Find(ecole => true).ToList();



      


        public ecole Get(string id) =>

              _ecole.Find<ecole>(eco => eco.Id == id).FirstOrDefault();


        //create Universty
        public ecole Create(ecole eco)
        {
            if (_ecole.Find<ecole>(Thiseco => Thiseco.nom ==eco.nom).FirstOrDefault() == null)
            {
                _ecole.InsertOne(eco);

                return eco;
            }
            else
            {
                return null;
            }

        }


        //delete uni
        public void Remove(string id) =>
         _ecole.DeleteOne(eco => eco.Id == id);

        //update universty

        public void Update(string id, ecole ecole) =>
            _ecole.ReplaceOne(univ => univ.Id == id, ecole);


    }
}
