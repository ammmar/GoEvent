﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using BooksApiRest.Models;

namespace BooksApiRest.Services
{
    public class EventsServices
    {
        private readonly IMongoCollection<Events> _events;


        public EventsServices(IBookstoreDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);
            _events = database.GetCollection<Events>(settings.EventsCollectionName);
        }


        public List<Events> Get() =>
         _events.Find(eventes => true).ToList();
        // _clubs.Find<Club>(ThisClub => ThisClub.Email == club.Email);
        

    public Events Get(string id) =>

          _events.Find<Events>(evente => evente.Id == id).FirstOrDefault();


        //cretae
        public Events Create(Events events)
        {
            if (_events.Find<Events>(ThisEvent => ThisEvent.name == events.name).FirstOrDefault() == null)
            {
                _events.InsertOne(events);

                return events;
            }
            else
            {
                return null;
            }

        }

        //update 1

        public void Update(string id, Events eventIn) =>
            _events.ReplaceOne(evente => evente.Id == id, eventIn);

        public void Remove(Events eventIn) =>
            _events.DeleteOne(evente => evente.Id == eventIn.Id);

        public void Remove(string id) =>
            _events.DeleteOne(evente => evente.Id == id);


    }
    }
