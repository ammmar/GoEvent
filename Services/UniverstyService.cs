﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using BooksApiRest.Models;

namespace BooksApiRest.Services
{
    public class UniverstyService
    {
        private readonly IMongoCollection<Universty> _universty;
        public UniverstyService(IBookstoreDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);
            _universty = database.GetCollection<Universty>(settings.UniverstyCollectionName);

        }


        public List<Universty> Get() =>
         _universty.Find(universty => true).ToList();
        // _clubs.Find<Club>(ThisClub => ThisClub.Email == club.Email);


        public Universty Get(string id) =>

              _universty.Find<Universty>(uni=> uni.Id == id).FirstOrDefault();


        //create Universty
        public Universty Create(Universty universty)
        {
            if (_universty.Find<Universty>(ThisUni => ThisUni.nom == universty.nom).FirstOrDefault() == null)
            {
                _universty.InsertOne(universty);

                return universty;
            }
            else
            {
                return null;
            }

        }


        //delete uni
        public void Remove(string id) =>
         _universty.DeleteOne(uni => uni.Id == id);

        //update universty

        public void Update(string id, Universty universty) =>
            _universty.ReplaceOne(univ => univ.Id == id, universty);


    }
}
