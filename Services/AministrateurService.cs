﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BooksApiRest.Models;
using MongoDB.Driver;

namespace BooksApiRest.Services
{
    public class AministrateurService
    {
        private readonly IMongoCollection<Aministrateur> _admin;

        public AministrateurService(IBookstoreDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);
            _admin = database.GetCollection<Aministrateur>(settings.AministrateurCollectionName);
        }

        public List<Aministrateur> Get() =>
       _admin.Find(admin => true).ToList();

        public Aministrateur Auth(string email)
        {
            //  Aministrateur auth = _admin.Find<Aministrateur>(Thisadmin => Thisadmin.email == email).FirstOrDefault();
            List<Aministrateur> lista = this.Get();
            Console.Write("size count admintarteur " + lista.Count+"email"+email);
            //  Aministration auth = _adminstration.Find<Aministration>(Thisadmin => Thisadmin.email == email).FirstOrDefault();
            foreach (Aministrateur item in lista)
            {
                if (item.email == email)
                {
                    return item;
                }
            }
            return null;
        }

        public Aministrateur Get(string id) =>

        _admin.Find<Aministrateur>(admin => admin.Id == id).FirstOrDefault();



        public void Update(string id, Aministrateur adminIn) =>
            _admin.ReplaceOne(admin => admin.Id == id, adminIn);

    }
}
