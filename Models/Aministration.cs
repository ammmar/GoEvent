﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BooksApiRest.Models
{
    public class Aministration
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        public string nom { get; set; }
        public string prenom { get; set; }

        public string email { get; set; }

        public string password { get; set; }

        public string poste { get; set; }

        public string estAuthentifier { get; set; }
    }
}
