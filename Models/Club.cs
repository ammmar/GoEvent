﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BooksApiRest.Models;
namespace BooksApiRest.Models
{
    public class Club
    {
        //[BsonRepresentation(BsonType.ObjectId)]

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        public string Logo { get; set; }

        public string ClubName { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string Faculty { get; set; }

        public string Domaine { get; set; }
 
    }
}
