﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace BooksApiRest.Models
{
    public class Events
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string name { set; get; }
        public string description { set; get; }
        public string startDate { set; get; }
        public string startTime { set; get; }
        public string endDate { set; get; }
        public string endTime { set; get; }
        public string validationDate { set; get; }
        public string region { set; get; }
        public string location { set; get; }
      
    }
}
