﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BooksApiRest.Models
{
    public class BookstoreDatabaseSettings : IBookstoreDatabaseSettings
    {
        public string ClubsCollectionName { get; set; }
        public string SponsorsCollectionName { get; set; }

        public string UniverstyCollectionName { get; set; }

        public string EcoleCollectionName { get; set; }
        public string EventsCollectionName { get; set; }

        public string AministrateurCollectionName { get; set; }
        public string AministrationCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }

    public interface IBookstoreDatabaseSettings
    {
        public string ClubsCollectionName { get; set; }
        public string SponsorsCollectionName { get; set; }
        public string EventsCollectionName { get; set; }
        public string UniverstyCollectionName { get; set; }
        public string AministrateurCollectionName { get; set; }

        public string AministrationCollectionName { get; set; }

        public string EcoleCollectionName { get; set; }
      
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}
