﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BooksApiRest.Services;
using Microsoft.AspNetCore.Mvc;

using BooksApiRest.Models;
namespace BooksApiRest.Controllers
{
    [Route("api/adminstration")]
    [ApiController]
    public class AdminstrationController : Controller
    {
        private readonly ClubsService _clubsService;
        private readonly EventsServices _eventsService;

        private readonly UniverstyService _universtyService;

        private readonly EcoleService _ecoleService;

        private readonly AministrationService _adminstrationService;

        public AdminstrationController(ClubsService clubsService, EventsServices eventsServices, UniverstyService universtyService, EcoleService ecoleService, AministrationService adminstrateurService)
        {
            _clubsService = clubsService;
            _eventsService = eventsServices;
            _universtyService = universtyService;
            _ecoleService = ecoleService;
            _adminstrationService = adminstrateurService;
        }



        [HttpGet]
        [HttpGet("clublist")]
        public ActionResult<List<Club>> Get() =>
        _clubsService.Get();


        [HttpGet]
        [HttpGet("eventlist")]
        public ActionResult<List<Events>> GetEvents() =>
        _eventsService.Get();



        //***************************************************Crud Uninversty*************************************


        //*****get all universty
        [HttpGet]
        [HttpGet("universtylist")]
        public ActionResult<List<Universty>> GetUniversty() =>
         _universtyService.Get();

        //*****create universty
        [HttpPost("createUniversty")]
        public ActionResult<Universty> CreateUniversty(Universty universty)
        {

            if (_universtyService.Create(universty) != null)
            {

                return new OkObjectResult(new { Id = 123, Name = "Universty created" + universty.Id + "universty name" + universty.nom,status=200 });
            }
            else
            {
                return new OkObjectResult(new { Id = 123, Name = "problem d'ajout universty" + universty.nom, status = 404 });
            }
        }

        //delete universty
        [HttpDelete("deleteuniversty/{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            var universty = _universtyService.Get(id);


            if (universty == null)
            {
                // Console.WriteLine("club found is "+club.ClubName);
                return new OkObjectResult(new { Id = 123, Name = "universty not found" + id ,status=404});

            }

            _universtyService.Remove(id);

            return new OkObjectResult(new { Id = 123, Name = "univetsty deletd " + id , status = 200});


        }

        //update universty


        [HttpPut("updateuniversty/{id:length(24)}")]
        public IActionResult Update(string id, Universty universty)
        {
            var uni = _universtyService.Get(id);

            if (uni == null)
            {
                return new OkObjectResult(new { Id = 123, Name = "y'a pas de universty avec ce id la " + id, status = 404 });
            }
            _universtyService.Update(id, universty);
            return new OkObjectResult(new { Id = 123, Name = "success universty update " + id , status = 200});
        }


        //***************************************************Crud ecole*************************************
        //*****get all universty
        [HttpGet]
        [HttpGet("ecolelist")]
        public ActionResult<List<ecole>> GetEcole() =>
         _ecoleService.Get();


        //*****create ecole
        [HttpPost("createEcole")]
        public ActionResult<Universty> CreateEcole(ecole ecole)
        {

            if (_ecoleService.Create(ecole) != null)
            {

                return new OkObjectResult(new { Id = 123, Name = "Ecole created" + ecole.Id + "ecole name" + ecole.nom, status = 200 });
            }
            else
            {
                return new OkObjectResult(new { Id = 123, Name = "problem d'ajout ecole" + ecole.nom, status = 404 });
            }
        }


        //delete universty
        [HttpDelete("deleteEcole/{id:length(24)}")]
        public IActionResult DeleteEcole(string id)
        {
            var eco = _ecoleService.Get(id);


            if (eco == null)
            {
                // Console.WriteLine("club found is "+club.ClubName);
                return new OkObjectResult(new { Id = 123, Name = "ecole not found" + id , status = 404 });

            }

            _ecoleService.Remove(id);

            return new OkObjectResult(new { Id = 123, Name = "success ecole deletd " + id, status = 200 });


        }

        //update ecole


        [HttpPut("updateEcole/{id:length(24)}")]
        public IActionResult UpdateEcole(string id, ecole ecole)
        {
            var eco = _ecoleService.Get(id);

            if (eco == null)
            {
                return new OkObjectResult(new { Id = 123, Name = "y'a pas de école avec ce id la " + id , status = 404});
            }
            _ecoleService.Update(id, ecole);
            return new OkObjectResult(new { Id = 123, Name = "success ecole update " + id, status = 200 });
        }


        [HttpPost("Auth")]
        public ActionResult<Aministration> AuthAdminstration(Aministration admin)
        {
            var adminstration=_adminstrationService.Auth(admin.email);

            if (adminstration != null)
            {
                if (adminstration.password == admin.password)
                {
                    Aministration admins = _adminstrationService.Get(admin.Id);
                    admins.estAuthentifier = "true";
                    _adminstrationService.Update(admins.Id, admins);
                    return Ok(new { title = "Account found", status = 200 });

                }
                else
                {
                    return Ok(new { title = "Incorrect password", status = 403, email = admin.email});

                }
            }
            else
            {
                return NotFound(new { title = "Account not found", status = 404, email = admin.email });

            }

        }
            //******

            /*
    [HttpPost("Auth")]
    public ActionResult<Aministration> AuthAdminstration(Aministration admin)
    {
        _adminstrateurService.


            }
        if (myClub != null)
        {
            if (myClub.Password == club.Password)
            {
                Club clubDetail = _clubService.Get(myClub.Id);

                string clubToken = jwt.Authenticate(clubDetail);

                return Ok(new { title = "Account found", status = 200, clubDetail, token = clubToken });

            }
            else
            {
                return Ok(new { title = "Incorrect password", status = 404, email = club.Email });

            }
        }
        else
        {
            return NotFound(new { title = "Account not found", status = 404, email = club.Email });

        }*/
        }


}





