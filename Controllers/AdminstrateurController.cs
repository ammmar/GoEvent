﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BooksApiRest.Services;

using BooksApiRest.Models;
using Microsoft.AspNetCore.Authorization;



namespace BooksApiRest.Controllers
{
    [Route("api/admin")]
    [ApiController]
    public class AdminstrateurController : Controller
    {
        private readonly ClubsService _clubsService;
        private readonly EventsServices _eventsService;

        private readonly AministrateurService _adminstrateurService;


        public AdminstrateurController(ClubsService clubsService, EventsServices eventsServices, AministrateurService adminstrateurService)
        {
           _clubsService = clubsService;
            _eventsService = eventsServices;
            _adminstrateurService = adminstrateurService;
         
        }

        [HttpGet]
        [HttpGet("clublist")]
        public ActionResult<List<Club>> Get() =>
         _clubsService.Get();

        [HttpGet]
        [HttpGet("clublist/{id:length(24)}")]
        public ActionResult<Club> Get(string id) =>
        _clubsService.Get(id);


        [HttpGet]
        [HttpGet("eventlist")]
        public ActionResult<List<Events>> GetEvents() =>
        _eventsService.Get();


        //create club


        [HttpPost("createClub")]
        public ActionResult<Club> Create(Club club)
        {
            
            _clubsService.Create(club);

            return new OkObjectResult(new { Id = 123, Name = "Club created"+club.Id+"club name"+club.ClubName });
        }


        //delete club
        [HttpDelete("deletevent/{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            var evente = _eventsService.Get(id);
           
        
            if (evente == null)
            {
               // Console.WriteLine("club found is "+club.ClubName);
                return new OkObjectResult(new { Id = 123, Name = "event not found"+id });

             }

            _eventsService.Remove(id);

            Console.WriteLine("remove event service is ");
            return new OkObjectResult(new { Id = 123, Name = "event deletd "+id});
            /*  if (b=true)
              {
                  return new OkObjectResult(new { Id = 123, Name = "Club deleted" });
              }
              else
              {
                  return new OkObjectResult(new { Id = 123, Name = "probleme de suppression" });
              }*/

        }

        [HttpPost("Auth")]
        public ActionResult<Aministrateur> AuthAdminstration(Aministrateur admin)
        {
            var adminstrateur = _adminstrateurService.Auth(admin.email);

            if (adminstrateur != null)
            {
                if (adminstrateur.password == admin.password)
                {
                    Aministrateur admins = _adminstrateurService.Get(admin.Id);
                    admins.estAuthentifier = "true";
                    _adminstrateurService.Update(admins.Id, admins);
                    return Ok(new { title = "Account found", status = 200 });

                }
                else
                {
                    return Ok(new { title = "Incorrect password", status = 403, email = admin.email });

                }
            }
            else
            {
                return NotFound(new { title = "Account not found", status = 404, email = admin.email });

            }

        }




    }
}
